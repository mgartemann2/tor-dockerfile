FROM debian
MAINTAINER Matthia Ga

LABEL version="0.2"
LABEL description="Tor Node, 64bit, tor version: 0.2.7.6"

# basic install
RUN apt-get update && apt-get install -y \
  build-essential \
  libevent-dev \
  libssl-dev

# build tor
RUN mkdir /install
RUN mkdir -p /usr/local/etc/tor/
ADD tor-0.2.7.6.tar.gz /install/tor-0.2.7.6.tar.gz
WORKDIR /install/tor-0.2.7.6.tar.gz/tor-0.2.7.6
#RUN tar xfvz tor-0.2.7.6.tar.gz
#WORKDIR /install/tor-0.2.7.6.tar.gz/tor-0.2.7.6/
RUN ./configure
RUN make 

# cleanup
#RUN apt-get purge -y build-essential

# share config
VOLUME ["/usr/local/etc/tor/"]

EXPOSE 9001
EXPOSE 9030

ADD entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

