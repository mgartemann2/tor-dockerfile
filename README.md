# README 

## Build image
```sh 
$ ./build-image
```

## config
```sh
$ cp -R config-default/ conf
$ cp conf/torrc.sample conf/torrc
```

## run container, first start
```sh
$ ./run-container
```

## Enable autostart



